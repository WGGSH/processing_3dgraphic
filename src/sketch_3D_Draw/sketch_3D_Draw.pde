import processing.opengl.*;

Polar3D polar_camera=new Polar3D(700, 0, -PI/4);
PVector camera_position=new PVector(300, 300, 300);

PVector[][] vertex;
final int X_NUM=64;
final int Y_NUM=64;
final float range=250;
final float PI2=PI*2;


void vertex(PVector pos) {
  vertex(pos.x, pos.y, pos.z);
}

void setup() { 
  size(displayWidth, displayHeight, OPENGL);
  noFill();
  stroke(255, 0, 0);

  float phy;
  float sita;

  vertex=new PVector[X_NUM][Y_NUM];
  for (int i=0; i<X_NUM; i++) {
    for (int j=0; j<Y_NUM; j++) {
      vertex[i][j]=new PVector();

      phy=PI2/X_NUM*i;
      sita=PI/(Y_NUM-1)*j-PI/2;

      vertex[i][j].y=range*sin(sita);
      vertex[i][j].x=range*sin(phy*3)*cos(sita);
      vertex[i][j].z=range*sin(phy*2)*cos(sita);
    }
  }


  //noLoop();
}

void draw_Axis() {
  beginShape();

  stroke(255, 0, 0);
  vertex(0, 0, 0);
  vertex(1000, 0, 0);
  vertex(0, 0, 0);

  stroke(128, 0, 0);
  vertex(0, 0, 0);
  vertex(-1000, 0, 0);
  vertex(0, 0, 0);

  stroke(0, 255, 0);
  vertex(0, 0, 0);
  vertex(0, 1000, 0);
  vertex(0, 0, 0);

  stroke(0, 128, 0);
  vertex(0, 0, 0);
  vertex(0, -1000, 0);
  vertex(0, 0, 0);

  stroke(0, 0, 255);
  vertex(0, 0, 0);
  vertex(0, 0, 1000);
  vertex(0, 0, 0);

  stroke(0, 0, 128);
  vertex(0, 0, 0);
  vertex(0, 0, -1000);
  vertex(0, 0, 0);

  endShape();
}

void loop_Set() {

  background(0);

  //軸の描画
  draw_Axis();

  //座標を中心に
  translate(width/2, height/2, -20);

  //座標計算
  camera_move();
  camera_position=polar_camera.ToPVec();

  //ライティング
  ambientLight(63, 31, 31);
  directionalLight(255, 255, 255, -camera_position.x, -camera_position.y, -camera_position.z);
  pointLight(63, 127, 255, camera_position.x, camera_position.y, 200);
  //カメラ
  camera(camera_position.x, camera_position.y, camera_position.z, // 視点X, 視点Y, 視点Z
  0.0, 0.0, 0.0, // 中心点X, 中心点Y, 中心点Z
  0.0, 1.0, 0.0);
}

void camera_move() {
  //キーが押されていたら
  if (keyPressed==true) {
    switch(key) {
    case 'd':
      polar_camera.phy-=PI2/360;
      break;
    case 'a':
      polar_camera.phy+=PI2/360;
      break;
    case 'w':
      polar_camera.sita-=PI2/360;
      break;
    case 's':
      polar_camera.sita+=PI2/360;
      break;
    case 'i':
      polar_camera.r-=10.0f;
      break;
    case 'k':
      polar_camera.r+=10.0f;
      break;
    case 'z':
      count++;
      break;
    case 'x':
      count--;
      break;
    case 'j':
      count2--;
      break;
    case 'l':
      count2++;
      break;
    default:
      break;
    }
  }
}

int count=0;
int count2=0;

void draw() {     
  loop_Set();

  //座標の更新
  //count++;
  float phy, sita;
  for (int i=0; i<X_NUM; i++) {
    for (int j=0; j<Y_NUM; j++) {

      phy=PI2/X_NUM*i;
      sita=PI/(Y_NUM-1)*j-PI/2;

      vertex[i][j].y=range*sin(sita+PI2*sin(PI/1080*count2));
      vertex[i][j].x=range*sin(phy*2+PI2*sin(PI/360*count2))*cos(sita+PI2/360*count);
      vertex[i][j].z=range*sin(phy*3+PI2*sin(PI/360*count2))*cos(sita+PI2/360*count);
    }
  }

  colorMode(HSB, 360, 100, 100);

  stroke(255, 255, 255);
  noStroke();
  beginShape(TRIANGLE);
  int x1, x2, y1, y2;
  for (int i=0; i<X_NUM; i++) {
    for (int j=0; j<Y_NUM; j++) {
      x1=i;
      x2=i+1;
      if (x2>=X_NUM)x2=0;
      y1=j;
      y2=j+1;
      if (y2>=Y_NUM)y2=j;
      fill(360.0/((X_NUM))*(i), 100, 100);
      vertex(vertex[x1][y1]);
      vertex(vertex[x1][y2]);
      vertex(vertex[x2][y1]);
      vertex(vertex[x2][y2]);
      vertex(vertex[x1][y2]);
      vertex(vertex[x2][y1]);
    }
  }
  endShape(CLOSE);
}


class Polar3D{
  float r;
  float phy;
  float sita;
  
  //引数3つのコンストラクタ
  //各要素に引数を代入する
  Polar3D(float l,float p,float s){
    r=l;
    phy=p;
    sita=s;
  }
  //引数なしのコンストラクタ
  //各要素を0で初期化する
  Polar3D(){
    r=0;
    phy=0;
    sita=0;
  }
  
  //代入関数
  void Set(float l,float p,float s){
    r=l;
    phy=p;
    sita=s;
  }
  
  //長さを変更する関数
  void Set_R(float value){
    r=value;
  }
  
  //φを変更する関数
  void Set_Phy(float value){
    phy=value;
  }
  
  //θを変更する関数
  void Set_Sita(float value){
    sita=value;
  }
  
  //加算関数
  void Add(Polar3D pol){
    r+=pol.r;
    phy+=pol.phy;
    sita+=pol.sita;
  }
  
  void Add(float l,float p,float s){
    r+=l;
    phy+=p;
    sita+=s;
  }
  
  //減算関数
  void Sub(Polar3D pol){
    r-=pol.r;
    phy-=pol.phy;
    sita-=pol.sita;
  }
  
  void Sub(float l,float p,float s){
    r-=l;
    phy-=p;
    sita-=s;
  }
  
  PVector ToPVec(){
    PVector R=new PVector();
    R.x=r*cos(phy)*cos(sita);
    R.y=r*sin(sita);
    R.z=r*sin(phy)*cos(sita);
    return R;
  }
  
}
